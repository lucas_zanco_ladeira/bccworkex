import random
from gmpy2 import mpz
import gmpy2
from codecrypt.keyGen import randomMatrix
import math

def encryptString(pk, pin):

    pinCrypt = ""
    expandCrypt = ""

    for x in pin:
        bytearray = str.encode(x)

        for y in bytearray:
            binFunc = bin(y)

            for z in binFunc:
                if z == 0:
                    enc0 = encrypt(pk, 0)
                    pinCrypt = pinCrypt + str(enc0) + "*"
                    expandCrypt = expandCrypt + str(expand(pk, enc0)) + "*"
                else:
                    enc1 = encrypt(pk, 1)
                    pinCrypt = pinCrypt + str(enc1) + "*"
                    expandCrypt = expandCrypt + str(expand(pk, enc1)) + "*"

    return pinCrypt, expandCrypt

def encrypt(pk, m):
    if m != 0 and m != 1:
        print('not a valid m')
        return 0

    alpha = pk.P._lambda
    matrix = [[0 for i in range(pk.P._beta)] for j in range(pk.P._beta)]
    for i in range(pk.P._beta):
        for j in range(pk.P._beta):
            matrix[i][j] = random.randint(0, 2**alpha -1)

    pprime = 2**pk.P._rho
    r = random.randint(-pprime, pprime)
    pkAsk = (pk.pkAsk).copy()
    x0 = pkAsk.pop(0)
    xi0 = pkAsk[::2]
    xj1 = pkAsk[1::2]

    somatorio = mpz(0)
    for i in range(pk.P._beta):
        for j in range(pk.P._beta):
            x = gmpy2.mul(xj1[j], xi0[i])
            somatorio += gmpy2.mul(matrix[i][j], x)
    c = (mpz(m)+2*r+2*somatorio)%x0
    return c


def expand(pk, cAsk):

    l = int(math.sqrt(pk.P._theta))
    y = [[0 for i in range(l)] for i in range(l)]
    kappa = pk.P._gamma+6

    n = 2
    gmpy2.get_context().precision=n

    for i in range(l):
        for j in range(l):
            y[i][j] = float(gmpy2.mpfr(randomMatrix(i, j, kappa, pk.se, l)/(2**kappa)))
    y[0][0] = float(gmpy2.mpfr(pk.u11/2**kappa))

    gmpy2.get_context().precision=21000

    expand = [[1 for i in range(l)] for i in range(l)]
    for i in range(l):
        for j in range(l):
            expand[i][j]=float((cAsk * y[i][j])%2)

    return expand

def decrypt(sk, cAsk, z):

    soma = 0
    l = int(math.sqrt(sk.P._theta))
    for i in range(l):
        for j in range(l):
            soma += (sk.s0[i]*sk.s1[j]*z[i][j])

    soma = mpz(gmpy2.round_away(soma))
    m = (cAsk-soma)%2

    return m

def add(pk, c1, c2):
    soma = gmpy2.add(c1,c2)
    return soma%pk.pkAsk[0]

def mul(pk, c1, c2):
    mul = gmpy2.mul(c1,c2)
    return mul%pk.pkAsk[0]
