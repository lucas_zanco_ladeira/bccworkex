from rest_framework import status
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt

from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from homomorphic.models import Message
from homomorphic.modelSerializer import MessageSerializer
from codecrypt import encrypt, keyGen, fheKey, parameters

def index(request):
    return HttpResponse("Welcome!")

'''
Generate key pair
Type of request: POST
Parameters: size - toy/small/medium/large
'''
@api_view(['POST'])
@csrf_exempt
def keygen(request):

    size = request.POST['size']
    publickey, privatekey = keyGen.keygen(size)

    message.sks0 = privatekey.s0
    message.sks1 = privatekey.s1
    message.sktetha = privatekey.P._theta
    message.pkbeta = publickey.P._beta
    message.pklamba = publickey.P._lambda
    message.pkrho = publickey.P._rho
    message.pkask = publickey.pkAsk
    message.se = publickey.se
    message.u11 = publickey.u11
    message.sigma0 = publickey.sigma0
    message.sigma1 = publickey.sigma1

    serializer = MessageSerializer(message)
    return JSONResponse(serializer.data, status=status.HTTP_201_CREATED)

'''
Encrypt bit
Type of request: POST
Parameters: size - toy/small/medium/large
bit - to encrypt
pkAsk, se, u11, sigma0, sigma1 - public key
'''
@api_view(['POST'])
@csrf_exempt
def encrypt(request):

    size = request.POST['size']
    bit = request.POST['bit']
    pkAsk = request.POST['pkAsk']
    se = request.POST['se']
    u11 = request.POST['u11']
    sigma0 = request.POST['sigma0']
    sigma1 = request.POST['sigma1']
    P = parameters.par()
    P.setPar(size)
    fheKey.pk(pkAsk, se, u11, sigma0, sigma1, P)

    message = Message()
    message.messagecrypt = encrypt.encrypt(fheKey, bit)
    message.messageexpand = encrypt.expand(fheKey, message.messagecrypt)

    serializer = MessageSerializer(message)
    return JSONResponse(serializer.data, status=status.HTTP_201_CREATED)

'''
Decrypt bit
Type of request: POST
Parameters: size - toy/small/medium/large
s0, s1 - private key
z - expanded cipher
'''
@api_view(['POST'])
@csrf_exempt
def decrypt(request):

    size = request.POST['size']
    mes = request.POST['message']
    s0 = request.POST['s0']
    s1 = request.POST['s1']
    z = request.POST['z']
    P = parameters.par()
    P.setPar(size)
    fheKey.sk(s0, s1, P)

    message = Message()
    message.message = encrypt.decrypt(fheKey, mes, z)

    serializer = MessageSerializer(message)
    return JSONResponse(serializer.data, status=status.HTTP_201_CREATED)

'''
Generate key pair, encrypt message
Type of request: POST
Parameters:
- size - toy/small/medium/large
- message - string
'''
@api_view(['POST'])
@csrf_exempt
def message(request, email, name):

    message = Message()
    mes = request.POST['message']
    size = request.POST['size']
    publickey, privatekey = keyGen.keygen(size)

    message.sks0 = privatekey.s0
    message.sks1 = privatekey.s1
    message.sktetha = privatekey.P._theta
    message.pkbeta = publickey.P._beta
    message.pklamba = publickey.P._lambda
    message.pkrho = publickey.P._rho
    message.pkask = publickey.pkAsk
    message.se = publickey.se
    message.u11 = publickey.u11
    message.sigma0 = publickey.sigma0
    message.sigma1 = publickey.sigma1

    message.pincrypt, message.pinexpand = encrypt.encryptString(publickey, mes)

    serializer = MessageSerializer(message)
    return JSONResponse(serializer.data, status=status.HTTP_201_CREATED)

class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)