from rest_framework import serializers
from homomorphic.models import Message

class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
